const express = require('express')
const app = express()
const port = 3000
const mylib = require('./mylib')

// endpoint localhost:3000/
app.get('/', (req, res) => {
    res.send('Hello World!')
  })

// endpoint localhost:3000/add?a=42&b=21
app.get('/add', (req, res) => {
    const a = req.query.a
    const b = req.query.b
    console.log({ a, b })
    const total = mylib.sum(a,b)
    res.send('add works ' + total.toString())
})

// endpoint localhost:3000/mult?a=2&b=3
app.get('/mult', (req, res) => {
  const a = req.query.a
  const b = req.query.b
  console.log({a, b})
  const rezult = mylib.kerto(a,b)
  res.send('mult works ' + rezult.toString())
})



app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})