const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const mylib = require('../src/mylib')

describe('Unit testing mylib.js', () => {
    
    let myvar = undefined;

    // run tasks before tests in this block
    before(() => {
        myvar = 1; // setup before testing
    })
    
    // testit:

    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1,1)
        expect(result).to.equal(2)
    })

    it.skip('Assert foo is not bar', () => {
        assert('foo' !== 'bar')
    })

    it('Myvar should exist', () => {
        should.exist(myvar)
    })

    // tulee testien jälkeen:
    after ( () => {
        console.log('hello')
    })
})